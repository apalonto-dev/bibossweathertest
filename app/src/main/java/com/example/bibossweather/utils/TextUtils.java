package com.example.bibossweather.utils;

import com.example.bibossweather.models.CityWeather;

import java.util.List;

public class TextUtils {

	public static String getCitiesAsArray(List<CityWeather> cities) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < cities.size(); i++) {
			sb.append(String.valueOf(cities.get(i).getId()));
			if (i != cities.size() - 1) {
				sb.append(",");
			}
		}
		return sb.toString();
	}
}