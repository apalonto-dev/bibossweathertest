package com.example.bibossweather.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitController {

    static final String BASE_URL = "http://api.openweathermap.org/";

    private static OpenWeatherMapApi openWeatherMapApi;

    public static OpenWeatherMapApi getApi() {
		if (openWeatherMapApi == null) {
			Gson gson = new GsonBuilder()
					.setLenient()
					.create();

			Retrofit retrofit = new Retrofit.Builder()
					.baseUrl(BASE_URL)
					.addConverterFactory(GsonConverterFactory.create(gson))
					.build();

			openWeatherMapApi = retrofit.create(OpenWeatherMapApi.class);
		}
        return openWeatherMapApi;
    }
}