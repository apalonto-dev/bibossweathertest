package com.example.bibossweather.utils;

import android.app.Activity;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.example.bibossweather.R;
import com.example.bibossweather.dialogs.MyAlertDialog;

public class MySnackBar implements View.OnClickListener {

	private Snackbar snackbar;
	private View parentView;
	private int duration = Snackbar.LENGTH_LONG;
	private Activity activity;
	private String message;

	public MySnackBar(Activity activity, View parentView) {
		this.activity = activity;
		this.parentView = parentView;
	}

	public void showPositive(String message) {
		this.message = message;
		snackbar = Snackbar.make(parentView, message, duration);
		View view = snackbar.getView();
		view.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorPrimary));
		TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
		tv.setTextColor(Color.WHITE);
		tv.setMaxLines(999);
		view.setOnClickListener(this);
		snackbar.show();
	}

	public void showNegative(String message) {
		this.message = message;
		snackbar = Snackbar.make(parentView, message, duration);
		View view = snackbar.getView();
		view.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorAccent));
		TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
		tv.setTextColor(Color.WHITE);
		tv.setMaxLines(999);
		view.setOnClickListener(this);
		snackbar.show();
	}

	public void showPermission(String message, final String[] permissions, final int code) {
		this.message = message;
		snackbar = Snackbar.make(parentView, message, Snackbar.LENGTH_INDEFINITE);
		snackbar.setAction("ОК", new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				ActivityCompat.requestPermissions(activity, permissions,
						code);
			}
		});
		snackbar.setActionTextColor(Color.BLACK);
		View view = snackbar.getView();
		TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
		view.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorAccent));
		tv.setTextColor(Color.WHITE);
		tv.setMaxLines(999);
		view.setOnClickListener(this);
		snackbar.show();
	}

	public void showPermissionForFragment(final Fragment fragment, String message,
										  final String[] permissions, final int code) {
		this.message = message;
		snackbar = Snackbar.make(parentView, message, Snackbar.LENGTH_INDEFINITE);
		snackbar.setAction("ОК", new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				fragment.requestPermissions(permissions, code);
			}
		});
		snackbar.setActionTextColor(Color.BLACK);
		View view = snackbar.getView();
		TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
		view.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorAccent));
		tv.setTextColor(Color.WHITE);
		tv.setMaxLines(999);
		view.setOnClickListener(this);
		snackbar.show();
	}

	@Override
	public void onClick(View view) {
		MyAlertDialog dialog = new MyAlertDialog(activity, message);
		dialog.show();
	}
}