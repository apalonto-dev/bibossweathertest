package com.example.bibossweather.utils;

import com.example.bibossweather.models.CitiesWeatherResponse;
import com.example.bibossweather.models.CityWeather;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OpenWeatherMapApi {

    @GET("/data/2.5/weather")
    Call<CityWeather> getCityWeather(@Query("q") String cityName,
									 @Query("units") String units,
									 @Query("appid") String apiKey);

    @GET("/data/2.5/group")
	Call<CitiesWeatherResponse> getCitiesWeather(@Query("id") String cityIds,
												 @Query("units") String units,
												 @Query("appid") String apiKey);

	@GET("/data/2.5/weather?units=metric&lang=ru")
	Call<CityWeather> getWeatherByCoords(@Query("appid") String apikey,
												  @Query("lat") double lat,
												  @Query("lon") double lon);
}