package com.example.bibossweather.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.example.bibossweather.R;

public class MyAlertDialog extends AlertDialog implements DialogInterface.OnClickListener {

	public MyAlertDialog(Context context, String message) {
		super(context);
		setMessage(message);
		setButton(AlertDialog.BUTTON_POSITIVE, context.getString(R.string.ok), this);
	}

	@Override
	public void onClick(DialogInterface dialogInterface, int i) {
		dialogInterface.dismiss();
	}
}