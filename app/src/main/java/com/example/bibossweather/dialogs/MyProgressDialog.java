package com.example.bibossweather.dialogs;

import android.app.ProgressDialog;
import android.content.Context;

import com.example.bibossweather.R;

public class MyProgressDialog extends ProgressDialog {

    public MyProgressDialog(Context context) {
        super(context);
        setMessage(context.getString(R.string.loading));
        setCancelable(false);
    }
}