package com.example.bibossweather.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.example.bibossweather.R;
import com.example.bibossweather.dao.CityDAO;
import com.example.bibossweather.dao.CityDAOImpl;
import com.example.bibossweather.listeners.ScrollListener;
import com.example.bibossweather.models.CitiesWeatherResponse;
import com.example.bibossweather.ui.adapters.CityAdapter;
import com.example.bibossweather.models.CityWeather;
import com.example.bibossweather.utils.Logger;
import com.example.bibossweather.utils.OpenWeatherMapApi;
import com.example.bibossweather.utils.RetrofitController;
import com.example.bibossweather.utils.TextUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

	private static final String TAG = "bibossweather.MainActivity";

    private OpenWeatherMapApi openWeatherMapApi;

    private RecyclerView mRecyclerView;

    private CityAdapter mAdapter;

    private List<CityWeather> citiesWeather;

	private CityDAO cityDAO = CityDAOImpl.getInstance();

	private SwipeRefreshLayout swipeRefreshLayout;

	private FloatingActionButton fab;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tbMain);
        setSupportActionBar(toolbar);

		mRecyclerView = (RecyclerView) findViewById(R.id.rvCity);
		fab = (FloatingActionButton) findViewById(R.id.fab);
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);

		citiesWeather = new ArrayList<>();

		citiesWeather.addAll(cityDAO.getCities(this));

		if (citiesWeather.size() == 0) {
			goToAddCity();
			finish();
			return;
		}

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddCityActivity.class);
				intent.putExtra("show_home", true);
                startActivity(intent);
            }
        });

		mRecyclerView.addOnScrollListener(new ScrollListener(fab));

        openWeatherMapApi = RetrofitController.getApi();

        mAdapter = new CityAdapter(this, citiesWeather);
        mRecyclerView.setAdapter(mAdapter);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setItemAnimator(itemAnimator);

		swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
		swipeRefreshLayout.setOnRefreshListener(refreshContent);
    }

    private void getCitiesWeather() {
		openWeatherMapApi.getCitiesWeather(TextUtils.getCitiesAsArray(citiesWeather), "metric",
				getString(R.string.api_key)).enqueue(cityWeatherCallback);
    }

    private Callback<CitiesWeatherResponse> cityWeatherCallback = new Callback<CitiesWeatherResponse>() {
		@Override
		public void onResponse(Call<CitiesWeatherResponse> call, Response<CitiesWeatherResponse> response) {
			Logger.withTag(TAG).log("CitiesWeatherResponse onResponse");
			swipeRefreshLayout.setRefreshing(false);
			if (response.isSuccessful()) {
				CitiesWeatherResponse mResponse = response.body();
				List<CityWeather> loadedWeather = mResponse.getCityWeathers();

				cityDAO.updateCities(MainActivity.this, loadedWeather);
				citiesWeather.clear();
				citiesWeather.addAll(loadedWeather);
				mAdapter.notifyDataSetChanged();
			}
		}

		@Override
		public void onFailure(Call<CitiesWeatherResponse> call, Throwable t) {
			Logger.withTag(TAG).log("CitiesWeatherResponse onFailure").withCause(t);
			swipeRefreshLayout.setRefreshing(false);
			Toast.makeText(MainActivity.this, "An error occurred during networking", Toast.LENGTH_SHORT).show();
		}
	};

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		citiesWeather.clear();
		citiesWeather.addAll(cityDAO.getCities(this));
		mAdapter.notifyDataSetChanged();
	}

	private void goToAddCity() {
		startActivity(new Intent(this, AddCityActivity.class));
	}

	private SwipeRefreshLayout.OnRefreshListener refreshContent = new SwipeRefreshLayout.OnRefreshListener() {
		@Override
		public void onRefresh() {
			getCitiesWeather();
		}
	};

	public void notifyItemRemoved() {
		fab.show();
	}
}