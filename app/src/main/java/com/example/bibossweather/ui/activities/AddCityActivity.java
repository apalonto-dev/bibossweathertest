package com.example.bibossweather.ui.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.example.bibossweather.R;
import com.example.bibossweather.dao.CityDAOImpl;
import com.example.bibossweather.dialogs.MyAlertDialog;
import com.example.bibossweather.dialogs.MyProgressDialog;
import com.example.bibossweather.exceptions.PermissionException;
import com.example.bibossweather.location.MyLocationListener;
import com.example.bibossweather.location.MyLocationManager;
import com.example.bibossweather.models.CityWeather;
import com.example.bibossweather.utils.Logger;
import com.example.bibossweather.utils.MySnackBar;
import com.example.bibossweather.utils.OpenWeatherMapApi;
import com.example.bibossweather.utils.RetrofitController;

import java.security.ProviderException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddCityActivity extends AppCompatActivity {

	private static final int REQUEST_LOCATION = 1;

	private static final String TAG = "AddCityActivity";

	private MyProgressDialog progressDialog;

	private EditText etName;

	private OpenWeatherMapApi openWeatherMapApi;

	private MyLocationManager locationManager;

	private MySnackBar snackBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_city);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tbAddCity);
        setSupportActionBar(toolbar);

		if (getIntent().getBooleanExtra("show_home", false)) {
			getSupportActionBar().setDisplayShowHomeEnabled(true);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}

        getSupportActionBar().setTitle(getResources().getString(R.string.add_city));

		etName = (EditText) findViewById(R.id.et_name);
		findViewById(R.id.btn_add).setOnClickListener(addListener);
		findViewById(R.id.btn_loc).setOnClickListener(byLocationListener);

		progressDialog = new MyProgressDialog(this);

		openWeatherMapApi = RetrofitController.getApi();

		snackBar = new MySnackBar(this, findViewById(R.id.main_view));
		locationManager = new MyLocationManager(this, new LocationChanged());

    }

    private View.OnClickListener addListener = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			String name = etName.getText().toString();
			if (name.length() > 0) {
				addCity(name);
			}
		}
	};

	private void addCity(String name) {
		openWeatherMapApi.getCityWeather(name, "metric",
				getString(R.string.api_key)).enqueue(cityWeatherCallback);
		progressDialog.show();
	}

	private Callback<CityWeather> cityWeatherCallback = new Callback<CityWeather>() {
		@Override
		public void onResponse(Call<CityWeather> call, Response<CityWeather> response) {
			Logger.withTag(TAG).log("CitiesWeatherResponse onResponse");
			progressDialog.dismiss();
			if (response.isSuccessful()) {
				CityWeather cityWeather = response.body();
				CityDAOImpl.getInstance().addCity(AddCityActivity.this, cityWeather);
				goToMain();
			}
		}

		@Override
		public void onFailure(Call<CityWeather> call, Throwable t) {
			Logger.withTag(TAG).log("CitiesWeatherResponse onFailure").withCause(t);
			progressDialog.dismiss();
			new MyAlertDialog(AddCityActivity.this, getString(R.string.connection_error)).show();
		}
	};

	private void goToMain() {
		startActivity(new Intent(this, MainActivity.class));
		finish();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
		if (menuItem.getItemId() == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(menuItem);
	}

	private class LocationChanged extends MyLocationListener {

		@Override
		public void onLocationChanged(Location location) {
			Logger.withTag(TAG).log("location changed");
			progressDialog.dismiss();
			openWeatherMapApi.getWeatherByCoords(getString(R.string.api_key),
					location.getLatitude(), location.getLongitude()).enqueue(cityWeatherCallback);
		}
	}

	private View.OnClickListener byLocationListener = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			try {
				locationManager.requestSingleUpdateWithNetworkProvider();
				progressDialog.show();
			} catch (ProviderException e) {
				e.printStackTrace();
				locationManager.getLocationDialogBuilder().show();
			} catch (PermissionException e) {
				e.printStackTrace();
				requestGpsPermission();
			}
		}
	};

	@Override
	protected void onDestroy() {
		super.onDestroy();
		locationManager.removeUpdate();
	}

	@SuppressLint("NewApi")
	private void requestGpsPermission() {
		String[] PERMISSIONS_LOCATION = {Manifest.permission.ACCESS_FINE_LOCATION,
				Manifest.permission.ACCESS_COARSE_LOCATION};
		if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)
				|| shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)) {
			snackBar.showPermission("Внимание!\nДля автоматического поиска вашего города"
					+ " необходимо разрешить определение местоположения.", PERMISSIONS_LOCATION, REQUEST_LOCATION);
		} else {
			requestPermissions(PERMISSIONS_LOCATION, REQUEST_LOCATION);
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
										   @NonNull int[] grantResults){
		if (requestCode == REQUEST_LOCATION) {
			if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED
					&& grantResults[1] == PackageManager.PERMISSION_GRANTED) {
				tryRequestForUpdate();
			} else {
				String[] PERMISSIONS_LOCATION = {Manifest.permission.ACCESS_FINE_LOCATION,
						Manifest.permission.ACCESS_COARSE_LOCATION};
				snackBar.showPermission("Внимание!\nДля автоматического поиска вашего города"
						+ " необходимо разрешить определение местоположения.", PERMISSIONS_LOCATION, REQUEST_LOCATION);
			}
		} else {
			super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		}
	}

	private void tryRequestForUpdate() {
		try {
			locationManager.requestSingleUpdateWithNetworkProvider();
			progressDialog.show();
		} catch (ProviderException | PermissionException e) {
			e.printStackTrace();
		}
	}
}