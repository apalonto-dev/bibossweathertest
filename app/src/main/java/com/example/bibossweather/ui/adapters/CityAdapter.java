package com.example.bibossweather.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bibossweather.R;
import com.example.bibossweather.dao.CityDAOImpl;
import com.example.bibossweather.models.CityWeather;
import com.example.bibossweather.ui.activities.MainActivity;

import java.util.List;

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.ViewHolder> {

	private Context context;

    private List<CityWeather> cities;

    public CityAdapter(Context context, List<CityWeather> cities) {
		this.context = context;
		this.cities = cities;
    }

    @Override
    public CityAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city, parent, false);
		return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CityWeather cityWeather = cities.get(position);
        holder.tvName.setText(cityWeather.getName());
		String temp = Math.round(cityWeather.getMain().getTemp()) + "°C";
        holder.tvTemperature.setText(temp);
		String windSpeed = "Скорость ветра: " + Math.round(cityWeather.getWind().getSpeed()) + "м/с";
        holder.tvWindSpeed.setText(windSpeed);
        String windDirection = "Ветер: " + String.valueOf(headingToString(cityWeather.getWind().getDeg()));
        holder.tvWindDirection.setText(windDirection);
		holder.cityWeather = cityWeather;
    }

    @Override
    public int getItemCount() {
        return cities.size();
    }

	private static String headingToString(double x) {
		String directions[] = {"северный", "северо-восточный", "восточный", "юго-восточный", "южный", "юго-западный", "западный", "северо-западный", "северный"};
		return directions[(int)Math.round(((x % 360) / 45))];
	}

	class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

		private TextView tvName;
		private TextView tvTemperature;
		private TextView tvWindSpeed;
		private TextView tvWindDirection;

		private CityWeather cityWeather;

		private ViewHolder(View v) {
			super(v);
			tvName = (TextView) v.findViewById(R.id.tvCityName);
			tvTemperature = (TextView) v.findViewById(R.id.tvCityTemperature);
			tvWindSpeed = (TextView) v.findViewById(R.id.tvCityWindSpeed);
			tvWindDirection = (TextView) v.findViewById(R.id.tvCityWindDirection);

			v.findViewById(R.id.tv_delete).setOnClickListener(this);
		}

		@Override
		public void onClick(View view) {
			CityDAOImpl.getInstance().deleteCity(context, cityWeather.getId());
			cities.remove(cityWeather);
			notifyItemRemoved(getAdapterPosition());
			if (context instanceof MainActivity) {
				((MainActivity) context).notifyItemRemoved();
			}
		}
	}
}