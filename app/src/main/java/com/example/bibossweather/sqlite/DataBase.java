package com.example.bibossweather.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.bibossweather.domains.CityDomain;

public class DataBase extends SQLiteOpenHelper {

	private static final String NAME = "bibossweather_db";

	private static int DATABASE_VERSION = 1;

	public DataBase(Context context) {
		super(context, NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase sqLiteDatabase) {
		sqLiteDatabase.execSQL(CityDomain.CREATE_TABLE_SQL);
	}

	@Override
	public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
		sqLiteDatabase.execSQL("drop table if exists " +CityDomain.TABLE_NAME);
		onCreate(sqLiteDatabase);
	}
}