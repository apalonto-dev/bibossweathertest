package com.example.bibossweather.dao;

import android.content.Context;

import com.example.bibossweather.models.CityWeather;

import java.util.List;

public interface CityDAO {

	void addCity(Context context, CityWeather cityWeather);

	void addCities(Context context, List<CityWeather> cities);

	List<CityWeather> getCities(Context context);

	void updateCities(Context context, List<CityWeather> cities);

	void deleteCity(Context context, int id);
}