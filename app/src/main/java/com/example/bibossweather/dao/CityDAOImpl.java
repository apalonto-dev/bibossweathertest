package com.example.bibossweather.dao;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.bibossweather.domains.CityDomain;
import com.example.bibossweather.models.CityWeather;
import com.example.bibossweather.models.Main;
import com.example.bibossweather.models.Wind;
import com.example.bibossweather.sqlite.DataBase;

import java.util.ArrayList;
import java.util.List;

public class CityDAOImpl implements CityDAO {

	private static final CityDAOImpl ourInstance = new CityDAOImpl();

	public static CityDAOImpl getInstance() {
		return ourInstance;
	}

	private CityDAOImpl() {
	}

	@Override
	public void addCity(Context context, CityWeather cityWeather) {
		DataBase dataBase = new DataBase(context);
		SQLiteDatabase sqLiteDatabase = dataBase.getWritableDatabase();
		sqLiteDatabase.beginTransaction();
		try {
			sqLiteDatabase.insert(CityDomain.TABLE_NAME, null, cityToValues(cityWeather));
			sqLiteDatabase.setTransactionSuccessful();
		} finally {
			sqLiteDatabase.endTransaction();
			dataBase.close();
		}
	}

	@Override
	public void addCities(Context context, List<CityWeather> cities) {
		DataBase dataBase = new DataBase(context);
		SQLiteDatabase sqLiteDatabase = dataBase.getWritableDatabase();
		sqLiteDatabase.beginTransaction();
		try {
			for (CityWeather cityWeather : cities) {
				sqLiteDatabase.insert(CityDomain.TABLE_NAME, null, cityToValues(cityWeather));
			}
			sqLiteDatabase.setTransactionSuccessful();
		} finally {
			sqLiteDatabase.endTransaction();
			dataBase.close();
		}
	}

	@Override
	public List<CityWeather> getCities(Context context) {
		List<CityWeather> cities = new ArrayList<>();
		Cursor cursor = null;
		DataBase dataBase = new DataBase(context);
		try {
			cursor = dataBase.getReadableDatabase()
					.rawQuery("Select * from " +CityDomain.TABLE_NAME +" order by " +CityDomain.NAME, null);
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				CityWeather city = cursorToWeather(cursor);
				cities.add(city);
				cursor.moveToNext();
			}
		} finally {
			if (cursor != null)
				cursor.close();
			dataBase.close();
		}
		return cities;
	}

	@Override
	public void updateCities(Context context, List<CityWeather> cities) {
		DataBase dataBase = new DataBase(context);
		SQLiteDatabase sqLiteDatabase = dataBase.getWritableDatabase();
		sqLiteDatabase.beginTransaction();
		try {
			for (CityWeather city: cities) {
				sqLiteDatabase.update(CityDomain.TABLE_NAME, cityToValues(city), CityDomain.ID +" = " +city.getId(), null);
			}
			sqLiteDatabase.setTransactionSuccessful();
		} finally {
			sqLiteDatabase.endTransaction();
			dataBase.close();
		}
	}

	@Override
	public void deleteCity(Context context, int id) {
		DataBase dataBase = new DataBase(context);
		SQLiteDatabase sqLiteDatabase = dataBase.getWritableDatabase();
		sqLiteDatabase.beginTransaction();
		try {
			sqLiteDatabase.delete(CityDomain.TABLE_NAME, CityDomain.ID +" = " +id, null);
			sqLiteDatabase.setTransactionSuccessful();
		} finally {
			sqLiteDatabase.endTransaction();
			dataBase.close();
		}
	}

	private CityWeather cursorToWeather(Cursor cursor) {
		CityWeather cityWeather = new CityWeather();
		cityWeather.setId(cursor.getInt(0));
		cityWeather.setName(cursor.getString(1));
		Wind wind = new Wind();
		wind.setDeg(cursor.getDouble(2));
		wind.setSpeed(cursor.getDouble(3));
		cityWeather.setWind(wind);
		Main main = new Main();
		main.setTemp(cursor.getDouble(4));
		cityWeather.setMain(main);
		return cityWeather;
	}

	private ContentValues cityToValues(CityWeather city) {
		ContentValues values = new ContentValues();
		values.put(CityDomain.ID, city.getId());
		values.put(CityDomain.NAME, city.getName());
		values.put(CityDomain.WIND_DIRECTION, city.getWind().getDeg());
		values.put(CityDomain.WIND_SPEED, city.getWind().getSpeed());
		values.put(CityDomain.CURRENT_TEMPERATURE, city.getMain().getTemp());
		return values;
	}
}