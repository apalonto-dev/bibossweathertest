package com.example.bibossweather.exceptions;

public class PermissionException extends Exception {

    public PermissionException(){

    }

    public PermissionException(String message) {
        super(message);
    }
}
