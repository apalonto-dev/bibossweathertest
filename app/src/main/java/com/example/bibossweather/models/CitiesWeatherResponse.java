package com.example.bibossweather.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CitiesWeatherResponse {

	@SerializedName("cnt")
	private int count;

	@SerializedName("list")
	private List<CityWeather> cityWeathers;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<CityWeather> getCityWeathers() {
		return cityWeathers;
	}

	public void setCityWeathers(List<CityWeather> cityWeathers) {
		this.cityWeathers = cityWeathers;
	}
}