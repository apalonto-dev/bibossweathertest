package com.example.bibossweather.domains;

public class CityDomain {

	public static final String TABLE_NAME = "cities";

	public static final String ID = "id";
	public static final String NAME = "name";
	public static final String WIND_DIRECTION = "wind_direction";
	public static final String WIND_SPEED = "wind_speed";
	public static final String CURRENT_TEMPERATURE = "current_temperature";

	public static final String[] ALL_COLUMNS = { ID, NAME, WIND_DIRECTION, WIND_SPEED, CURRENT_TEMPERATURE };

	public static final String CREATE_TABLE_SQL = "create table " +TABLE_NAME +" (" +ID +" integer primary key,"
			+NAME +" text, " +WIND_DIRECTION +" real, " +WIND_SPEED +" real, " +CURRENT_TEMPERATURE +" real)";
}