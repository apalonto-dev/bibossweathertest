package com.example.bibossweather.listeners;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ScrollListener extends RecyclerView.OnScrollListener {

    private List<FloatingActionButton> buttons;

    public ScrollListener(List<FloatingActionButton> buttons) {
        this.buttons = buttons;
    }

    public ScrollListener(FloatingActionButton button) {
        buttons = new ArrayList<>();
        buttons.add(button);
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if (dy > 0) {
            hideButtons();
        } else if (dy < 0)
            showButtons();
    }

    private void hideButtons() {
        for (FloatingActionButton button: buttons) {
            button.hide();
        }
    }

    private void showButtons() {
        for (FloatingActionButton button: buttons) {
            button.show();
        }
    }
}
